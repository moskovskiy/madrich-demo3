import axios, { post } from 'axios';
import config from '../config.js';

function uploadConfigurationRequest (file) {
    //const url = "http://35.226.194.99:8000" + "/solver"
    const url = "http://desktop.dimitrius.club:8555" + "/solver" 
    //process.env.API_LOCATION /*"
    const formData = new FormData()

    formData.append('file', file)
    const config = {
        headers: {
            'content-type': 'multipart/form-data'
        }
    }
    return post(url, formData,config)
}

export default
function uploadConfiguration (file) {

    console.log("Starting file upload")

    uploadConfigurationRequest(file).then((response) => {
        let data = response.data
        console.log(data)
        localStorage.setItem('job_id', (data)?data.job_id:null)
        return data
    })
}
