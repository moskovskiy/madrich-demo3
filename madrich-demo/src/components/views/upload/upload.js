import React, {useState} from 'react';

import UploadIcon from './uploadIcon'
import UploadFile from '../../../api/uploadFile'

import "./upload.css";

function UploadDragAndDrop() {
  const [drag, setDrag] = useState(false);

  function DragStartHandler(e){
    e.preventDefault()
    setDrag(true)
  }
  function DragLeaveHandler(e){
    e.preventDefault()
    setDrag(false)
  }
  function onDropHandler(e){
    e.preventDefault()
    localStorage.setItem('job_id', null)
    let files = [...e.dataTransfer.files]
    UploadFile(files)
    setDrag(false)
  }


  return (
    <div className="upload-block__place-to-drag-and-drop">
     
      <div className="drop-area" style={{borderColor: (drag)?"#6DFFC2":"#D8D8D8"}}
        onDragStart = {e => DragStartHandler(e)}
        onDragLeave ={e => DragLeaveHandler(e)}
        onDragOver = {e => DragStartHandler(e)}
        onDrop = {e=> onDropHandler(e)}
      >
        <div className="drop-area-center">
        {drag 
          ? <>
              <UploadIcon color="#6DFFC2"/>
              <div className="upload-icon-text"> Опустите файлы,чтобы загрузить </div>
          </>
          : <>
              <UploadIcon color="#D8D8D8"/>
              <div className="upload-icon-text"> Загрузите файл, претащив его сюда (или выберите на устройстве вручную)</div>
            </>
        }
      
        </div>
      
        <br/>
        <UploadButton />
      </div>
    </div>
      
  );
}


function UploadBlock() {
  const [selec, setSelect] = useState(false);

  return(
    <div className="upload-block">
      <div className="upload-block__title">
        Загрузите файл с расширением
        <span className="upload-block__title_file"> .xlsx</span>
      </div>
      <UploadDragAndDrop />
      <DownloadButton />
    </div>
  );
}


function DownloadButton(){
  return(
    <a className="DownloadButton" href="http://desktop.dimitrius.club:8555/example" download={true}>
      Скачать демонстрационный файл .xlsx
    </a>
  );
}


function UploadButton(){
  function onChangeHandler(e){
    e.preventDefault()
    localStorage.setItem('job_id', null)
    let files = e.target.files[0]
    UploadFile(files)
  }
  return(
    <div className="example-2">
      <div className="form-group">
        <input type="file" accept=".xls,.xlsx" name="file" id="file" className="input-file"
          onChange = {e => onChangeHandler(e)}
        />
        <label htmlFor="file" className="btn btn-tertiary js-labelFile">

          <span className="js-fileName">Выбрать файл</span>
        </label>
    </div>
   </div>
  );
}

export default UploadBlock;
