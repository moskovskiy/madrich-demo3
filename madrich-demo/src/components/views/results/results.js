import "./results.css";
import React, { useState, useEffect } from 'react';
import Dropdown from '../../common/dropdown/dropdown';
import MapView from './map/map'
import Button from '../../common/button/button'
import ControlPanel from './controlpanel/controlPanel'
import RequestStatus from '../../../api/fetchResults'

function generateUTC (datestring) {
  let date = Date.parse(datestring)
  return date/1000
}

function generateColors () {
  let colors = []
  let t = 0;
        while (t < 100000) {
            t++
            let colorDigits = 100000 + ((100000 * Math.floor(Math.random()*90)) + (1000 * Math.floor(Math.random()*99)) + Math.floor(Math.random()*99))%89999
            let color = "#" + (colorDigits).toString(10)
            colors.push(color)
        }
}

function Resume(props) {

  //const [running, setSimulationRunning] = useState(false)
  //const [seconds, setTime] = useState(1601560189)

  //const [stwTimeStart, setTimeStart] = useState(0)

  const [status, setStatus] = useState({})
  const [action, setAction] = useState(0)

//  const [courier, setCourierNo] = useState(-1)
  const [showData, toggleShowData] = useState(false)

  let [seconds, setSeconds] = useState(1601553300)
  let [running, setRunning] = useState(false)
  let [step, setStep] = useState(1)

  let [retry, setRetry] = useState(0)
  let [disable, setDisable] = useState(false)

  useEffect(() => {
    let interval = setInterval(() => {
      setSeconds(seconds + step)
      setRetry (retry + 1)
      
      if (action != 2) {
        //console.log("Time in system: " + retry)
        //console.log("Server status request #" + retry)
        
        //alert (JSON.stringify(status.task_id))

        if ((retry % 100 == 0) && (!disable)) {
          console.log("REQUEST")
          try {RequestStatus(localStorage.getItem('job_id')).then(function (r) {
            setAction(1)
            //console.log("Attempting to get data with result ", r.data)
            setStatus(r.data)
            
            console.log("Status reponse =  ", status)
  
            if ((r.data) && (r.data.is_finished == true)) {
              setAction(2)
              setDisable(true)
            }
          })} catch (err) {
            alert(err)
          }
        }

      }

    }, 15)

    /*let id = setInterval(() => {
        if(!paused) setSeconds(seconds + step)
    }, 15)*/

    return () => {
      clearInterval(interval)
      //clearInterval(id)
    }
  }, [seconds, step, retry]);


  function calculateCouriers() {
    return (status.result.solved)?status.result.solved.length:0
  }

  function generateCouriers () {
    let arr = ["всех"]

    if (status.result.solved) {
      status.result.solved.map ((el, index) =>
        arr.push(el.name)
      )
    }
    
    return arr
  }
/*
  function selectCourier (index) {
    setCourierNo(Number(index)-1)
  }
*/
  let t_date = new Date(seconds * 1000)
  var t_day = t_date.getDay()
  var t_month = t_date.getMonth()
  var t_year = t_date.getUTCFullYear()
  var t_hours = t_date.getHours()
  var t_minutes = "0" + t_date.getMinutes()
  var t_seconds = "0" + t_date.getSeconds()
  let formattedTime = t_day + "." + t_month + "." + t_year + " " + t_hours + ':' + t_minutes.substr(-2) + ':' + t_seconds.substr(-2)

  return(
    <div className="resume">
      
        {((action == 0) || (JSON.stringify(status.task_id) == "")) && 
          <div className='row'>
            <div className="resume-title">Добавьте файл, чтобы рассчитать маршруты</div>
          </div>
        }

        {(action == 1) && 
          <div className='row'>

            <div className="resume-title">Подождите немного,<br/> идёт построение маршрутов</div>

            <div className="resume-status">
              <br/><b>Расчёт задачи в процессе</b><br/>Номер задачи в системе {JSON.stringify(status.task_id)}<br/>Текущий статус {JSON.stringify(status.error)}
            </div>
            <br/>
            
            <div class="loader">
                  <div class="loader-el loader-el-1"></div>
                  <div class="loader-el loader-el-2"></div>
                  <div class="loader-el loader-el-3"></div>
                  <div class="loader-el loader-el-4"></div>
            </div>

          </div>
        }
        
        {(action == 2) && 
           <div className='row'>
              <div className="resume-title">Расчет закончен</div>
              <br/>
              <div className="resume-subtitle">Информация о маршрутах</div>
              
              <div className="resume__left-col">
                  <table className="resume-title-item">
                    <tr>
                        <td className="caption">Курьеров</td>
                        <td className="dots"></td>
                        <td className="caption">{calculateCouriers()}</td>
                    </tr>
                  </table>
                  <table className="resume-title-item">
                    <tr>
                        <td className="caption">Стоимость ₽</td>
                        <td className="dots"></td>
                        <td className="caption">{Math.floor(status.result.statistics.cost)}</td>
                    </tr>
                  </table>
                  <table className="resume-title-item">
                    <tr>
                        <td className="caption">Дистанция (км)</td>
                        <td className="dots"></td>
                        <td className="caption">{Math.floor(status.result.statistics.distance/1000)}</td>
                    </tr>
                </table>
                <table className="resume-title-item">
                    <tr>
                      <td className="caption">Длительность (мин)</td>
                      <td className="dots"></td>
                      <td className="caption">{Math.floor(status.result.statistics.duration/60)}</td>
                    </tr>
                  </table>
              </div>
              
              <div className="resume__right-col">
              <table className="resume-title-item">
                <tr>
                    <td className="caption">В движении (мин)</td>
                    <td className="dots"></td>
                    <td className="caption">{Math.floor(status.result.statistics.times.driving/60)}</td>
                </tr>
              </table>
              <table className="resume-title-item">
                <tr>
                    <td className="caption">У клиента (мин)</td>
                    <td className="dots"></td>
                    <td className="caption">{Math.floor(status.result.statistics.times.serving/60)}</td>
                </tr>
              </table>
              <table className="resume-title-item">
                <tr>
                    <td className="caption">Ожидание (мин)</td>
                    <td className="dots"></td>
                    <td className="caption">{Math.floor(status.result.statistics.times.waiting/60)}</td>
                </tr>
              </table>
              <table className="resume-title-item">
                <tr>
                    <td className="caption">Перерыв (мин)</td>
                    <td className="dots"></td>
                    <td className="caption">{Math.floor(status.result.statistics.times.break/60)}</td>
                </tr>
              </table>
              </div>
              
              <br/>


              <br/>
              <div className="resume-subtitle">
                {(running)
                  ? <>Текущее время в симуляции {formattedTime}</>
                  : <>Предпросмотр движения на карте</>}
              </div>

            {!running && <button className="simulator-button" onClick={()=>{
                setRunning(!running)
            }} style={{width: 150}}> ▶️ Запустить</button>}
          
            {running && <button className="simulator-button" onClick={()=>{
                setSeconds(0)
                setStep(1)
                setRunning(!running)
            }} >✕</button>}

            {running && <button className="simulator-button" onClick={()=>setStep(step-1)}>◁</button>}
            {running && <button className="simulator-button" onClick={()=>setStep(0)}>●</button>}
            {running &&<button className="simulator-button" onClick={()=>setStep(step+1)}>▷</button>}

            {running && <> Скорость симуляции: {String(step)} минут в секунду</>}

            <br/>
            

              <MapView 
                selected={-1}
                colors={generateColors()}
                list={generateCouriers()}
                config={
                  status.result.solved
                }
                seconds={seconds}
                running={running}
              />
              
              {showData&&<>
              <div className="resume-status">
                <div>{status.result.solved.map(el => {
                  return <div>
                    <div className="resume-subtitle">Маршрут курьера {el.name}</div>
                    {el.stops.map((lt, index, arr) =>
                      <div>
                      ↓ <b>{lt.name}</b>: {lt.location.lat}, {lt.location.lon} <br/>
                      в {lt.time.arrival + " " + generateUTC(lt.time.arrival)}, {lt.time.departure + " " + generateUTC(lt.time.departure)} <br/> <br/>
                      </div>
                    )}
                    <br/>
                    <br/>
                  </div>
                }
                )}</div>
              </div></>}
              
              <Button onClick={()=>toggleShowData(!showData)}title={showData?"Скрыть маршруты":"Показать маршруты"}/>
              
          </div>
        }

        {(action != 0) && <>
        
        <br/><Button 
          title="Удалить задачу"
          onClick={()=> {
            localStorage.setItem('job_id', null)
            setAction(0)
            setStatus({})
          }
        }/>
        </>}
  
    </div>
  );
}

export default Resume;
