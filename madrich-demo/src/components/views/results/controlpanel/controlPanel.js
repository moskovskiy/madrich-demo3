//import Dropdown from '../../..common/dropdown/dropdown.js';
//import './css/dropdown.css';
//import Play from './icons/play.svg';
//import Pause from './icons/pause.svg';
//import Faster from './icons/faster.svg';
//import Slower from './icons/lower.svg';
import ImageButton from './imageButton.js'
import Dropdown from '../../../common/dropdown/dropdown.js'
import PlayIcon from './imageButton.js'
import React, {useState} from 'react'

function pasteSVG(arg) {
  return(
    <img  src={arg}  width="14px"/>
  );
}

function ControlPanel (props) {
  const [state, setState] = useState({
    isPlay: 1,
    clickCounterPlayPause: 0
  });

  function PlayPause(event) {
      event.preventDefault();
        //console.log(event.target);
        console.log(state);
      if(state.clickCounterPlayPause%2 != 0){
        var counter = state.clickCounterPlayPause+1;
        setState({ isPlay: 1,
        clickCounterPlayPause: counter });
      }else{
        var counter = state.clickCounterPlayPause+1;
        setState({ isPlay: 0,
        clickCounterPlayPause: counter });
      }
    }



  return(
    <div className="control-panel">
      <ImageButton title="Старт" ico={[<PlayIcon/>]}/>
      <ImageButton title="Замедлить" ico={[]}/>
      <ImageButton title="Ускорить" ico={[]}/>
      
      <Dropdown 
                title="Отображать всех"
                adder="Отображать "
                list={props.list}
                setSelected={props.setSelected}
                style = {{width: "280px"}}
      />
    </div>
  );
}

export default ControlPanel;
