import React from 'react'

export default
function ImageButton (props) {
	return (
		<svg
			fill={props.color}
			width="14px"
      		height="14px"
			className=""
			version="1.1"
			xmlns="http://www.w3.org/2000/svg"
			viewBox="0 0 14 14"
		>
			<g>
			<path d="M4,0.9C2.9,0.3,2,0.8,2,2.1v9.4c0,1.3,0.9,1.8,2,1.2l8.2-4.7c1.1-0.6,1.1-1.7,0-2.3L4,0.9z"/>
			</g>
		</svg>
	)
}
