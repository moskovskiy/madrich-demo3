import React, { useState, useEffect} from 'react'
import "./demo.css"
import L  from 'leaflet'
import { Map, MapContainer, TileLayer, Popup, Marker, CircleMarker, Polyline } from 'react-leaflet';

const defaultLatLng /*: LatLngTuple*/ = [55.763984, 37.597438];
const defaultLatLng2 /*: LatLngTuple*/ = [55.773984, 37.617438];

var latlngs = [
  [[55.763984, 37.597438],
  [55.773984, 37.617438]]
];

const zoom /*: number */ = 10.5;
const AccessToken /*: string*/ = "pk.eyJ1IjoibWFkcmljaGV4YW1wbGUxIiwiYSI6ImNrZzJnbTM5cjAwb2oydnRibHltZmgxYmwifQ.zOO74FePw-_iSyPC138Emg" 
const StyleID /*: string*/ = "madrichexample1/ckg2jfwhc0rzb19o8qjyhe5k2"


function Demo () {
    let [seconds, setSeconds] = useState(0)
    let [running, setRunning] = useState(false)
    let [paused, setPaused] = useState(false)
    let [step, setStep] = useState(1)
    let [line, showingLine] = useState(0)
    let [fixed, setFixed] = useState(0)

    useEffect(() => {
        let id = setInterval(() => {
            if(!paused) setSeconds(seconds + step)
        }, 13.3)
        return () => clearInterval(id);
      }, [seconds, step]);

    return (
        <div className="demo">

            {running && <>Время в системе: {seconds} Секунд</>}

            <br/>
            <button className="simulator-button" onClick={()=>{
                if(running) {
                    setSeconds(0)
                    setStep(1)
                }
                setRunning(!running)
            }} >{running? "✕" : "▶️"}</button>
            {running && <button className="simulator-button" onClick={()=>setStep(step-1)}>◁</button>}
            {running && <button className="simulator-button" onClick={()=>setStep(0)}>●</button>}
            {running &&<button className="simulator-button" onClick={()=>setStep(step+1)}>▷</button>}

            {running && <> Скорость симуляции: {step}</>}
            
            <MapContainer className="MapView" id="mapId"
                center={defaultLatLng}
                zoom={zoom}>

                <TileLayer
                    url={"https://api.mapbox.com/styles/v1/"+StyleID+"/tiles/{z}/{x}/{y}?access_token=" + AccessToken}
                    prefix="" attribution={"routing powered by Madrich"}>
                </TileLayer>


                <CircleMarker
                            eventHandlers={{
                              mouseover: (e) => {
                                setFixed(false)
                                console.log(e)
                                console.log('A')
                                showingLine(1)
                                setFixed(-1)
                                e.target.openPopup()
                              }, 
                              mouseout: (e) => {
                                console.log(e)
                                console.log('A')
                                showingLine(0)
                                e.target.closePopup()
                              },
                              click: (e) => {
                                setFixed(1)
                              }
                            }}

                              

                              fill={true}
                              fillColor="white"
                              center={defaultLatLng}
                              opacity={1}
                              radius={6}
                              color={line? "red":"black"}
                >
                  <Popup>
                      Курьера 1
                  </Popup>
                </CircleMarker>

                {((line == 1) || (fixed == 1)) && <Polyline 
                     opacity={0.8}
                     weight={2}
                     color="red"
                     positions={latlngs}
                />}

                <CircleMarker
                            eventHandlers={{
                              mouseover: (e) => {
                                setFixed(false)
                                console.log(e)
                                console.log('A')
                                showingLine(1)
                                setFixed(-1)
                                e.target.openPopup()
                              }, 
                              mouseout: (e) => {
                                console.log(e)
                                console.log('A')
                                showingLine(0)
                                e.target.closePopup()
                              },
                              click: (e) => {
                                setFixed(1)
                              }
                            }}

                              

                              fill={false}
                              fill={true}
                              fillColor="white"
                              center={defaultLatLng2}
                              opacity={1}
                              radius={6}
                              color={line? "red":"black"}
                >
                  <Popup>
                      Курьера 1
                  </Popup>
                </CircleMarker>
                </MapContainer>
                        
        </div>
    )
}

export default Demo