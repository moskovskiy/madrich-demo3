import React, { useState } from 'react';
import L  from 'leaflet'
import { MapContainer, TileLayer, Popup, Marker, CircleMarker, Polyline } from 'react-leaflet';
import MarkerClusterGroup from 'react-leaflet-markercluster';
import './map.css'


const depotMarkerIcon = require('../../../../assets/depot.svg') 

const defaultLatLng /*: LatLngTuple*/ = [55.763984, 37.597438];

const zoom /*: number */ = 10.5;
const AccessToken /*: string*/ = "pk.eyJ1IjoibWFkcmljaGV4YW1wbGUxIiwiYSI6ImNrZzJnbTM5cjAwb2oydnRibHltZmgxYmwifQ.zOO74FePw-_iSyPC138Emg" 
const StyleID /*: string*/ = "madrichexample1/ckg2jfwhc0rzb19o8qjyhe5k2"

export const depotIcon = new L.Icon({
   iconUrl: depotMarkerIcon,
   iconRetinaUrl: depotMarkerIcon,
   iconAnchor: [15, 15],
   popupAnchor: [0, 0],
   iconSize: [25, 25],
})

function generateUTC (datestring) {
   let date = Date.parse(datestring)
   return date/1000
}
 
function moreThanZero (input) {
   if (input < 0) return 0 
   return input
}

function calculateCoordinates (millisecond, trip) {

      let step = 1
      let second = millisecond/1000
   
      while (step < (trip.stops.length - 1)) {

         if (trip.stops[step]) {
            let departure = generateUTC(trip.stops[step].time.departure)
            let arrival = generateUTC(trip.stops[step].time.arrival)
            let nextarrival = generateUTC(trip.stops[step+1].time.arrival)
   
            //console.log("At", step, " ", arrival, departure, trip.stops[step])
         
            let k = (second - departure)/(nextarrival - departure)
            let newX = trip.stops[step].location.lat + k*(trip.stops[step+1].location.lat - trip.stops[step].location.lat)
            let newY = trip.stops[step].location.lon + k*(trip.stops[step+1].location.lon - trip.stops[step].location.lon)
      
            if ((second >= arrival) && (second < departure)) {
               return trip.stops[step].coordinates
            }
      
            if ((second >= departure && (second < nextarrival))) {
               //console.log("dot at ", [newX, newY])
               return [newX, newY]
            }
         } else {
            alert ("Errr " + JSON.stringify(trip.props[step].time))
         }
         
   
         step++
      }
      //console.log("dot at ", [trip.stops[0].location.lat, trip.stops[0].location.lon])
      return [trip.stops[0].location.lat, trip.stops[0].location.lon]
}

let colors = []

function getRandomInt(max) {
   return Math.floor(Math.random() * Math.floor(max));
}

function generateColors () {
      let t = 0;
      while (t < 100000) {
         t++
         let colorDigits = 100000 + getRandomInt(890000) + getRandomInt(9000) + getRandomInt(99)
         let color = "#" + (colorDigits).toString(10)
         colors.push(color)
      }
}
   
const MapView = (props) => {
   
   generateColors()

   let [selectedCourierIndex, setSelectedCourier] = useState(-1)
   let [clicked, setClicked] = useState(false)

   function setCourierIndex(index) {
      //alert(index)
      setSelectedCourier(index)
   }

   return (
    <div>      
      <div className="map-hint">{<>
         {(selectedCourierIndex == -1) && <>Наведите мышь на точку, чтобы увидеть маршрут</>}
         {(selectedCourierIndex != -1) && <>Отображается маршрут курьера {props.list[selectedCourierIndex + 1]}{!clicked ? <>, нажмите, чтобы зафиксировать</> : <>, проведите над любой точкой, чтобы снять выделение</>}</>}
      </>}</div>
      <MapContainer className="MapView" id="mapId"
         center={defaultLatLng}
         zoom={zoom}>
          
         {props.config.map((tourSchema, index, _) => {

            let cord = calculateCoordinates(props.seconds, tourSchema)
            try {
               let color = colors[index]//(index == selectedCourierIndex) ? colors[index] : "#000000"

               return (
               <>{/*((props.selected == -1) || (props.selected == index)) && */ <>

                  {props.running && 
                     <CircleMarker 
                     color={color}
                     center={new L.LatLng(cord[0], cord[1])}
                     radius={4}/>
                  }
               

                  {!props.running && tourSchema.stops.map ((lt, lindex, arr) =>
                        /*<MarkerClusterGroup
                           spiderfyDistanceMultiplier={1}
                           showCoverageOnHover={false}
                        >*/<>
                           <CircleMarker
                            
                              eventHandlers={{
                                 mouseover: (e) => {
                                    setClicked(false)
                                    setCourierIndex(index)
                                    e.target.openPopup()
                                 }, 

                                 mouseout: (e) => {
                                    if (!clicked) {
                                       setCourierIndex(-1)
                                    }

                                    e.target.closePopup()
                                 }, 

                                 click: (e) => {
                                    setCourierIndex(index)
                                    setClicked(true)
                                 }
                              }}

                              fill={true}
                              fillColor={color}
                              fillOpacity={1}
                              center={new L.LatLng(lt.location.lat, lt.location.lon)} opacity={1}
                              radius={2}
                              color={color}
                              key={lindex}
                           >
                              <Popup>
                                 <span>
                                    <b>Точка </b>{String(lt.name)}<br/>
                                    <b>Курьер </b> {String(props.list[index + 1])} посетит точку в {String(lt.time.arrival)}
                                    
                                    {/*<button onClick={()=>
                                       setCourierIndex(index)
                                    }>
                                       Весь маршрут
                                    </button>*/}
                                 </span>
                              </Popup>
                           </CircleMarker>
                        </>
                     )
                  }

                  {(props.running || (index == selectedCourierIndex)) && <Polyline 
                     opacity={0.8}
                     weight={1}
                     color={color}
                     positions={tourSchema.stops.map(waypoint => new L.LatLng(waypoint.location.lat, waypoint.location.lon))}>
                  </Polyline>}


               </>}</>
            )}
            catch (err) {
               console.log("ERROR is: " + err)
            }

            /*
            return(
            <>{(index == props.selected || props.selected == -1)&& <>
               {!props.running &&<CircleMarker color={props.colors[index]} center={calculateCoordinates(props.seconds, tourSchema)} radius={4}/>}
               {!props.running &&<Polyline opacity={0.15} weight={1} color={props.colors[index]} positions={tourSchema.map(waypoint => waypoint.coordinates)}>
               </Polyline>}

               
               {!props.running && tourSchema.stops.map ((element) =>
               //((element: TripPoint) => 
                     <>
                     <CircleMarker center={element.coordinates} opacity={0.2} radius={0.5} color={props.colors[index]}>
                     </CircleMarker>
                     </>
                  )
               }
               {props.running && tourSchema.stops.map((element) =>
                //(element: TripPoint) => 
                     <>
                     <CircleMarker center={element.coordinates} radius={1} color={props.colors[index]}>
                     </CircleMarker>
                     </>
                  )
               }
               
            </>
            }</>);*/
         })
        }

         <TileLayer
            url={"https://api.mapbox.com/styles/v1/"+StyleID+"/tiles/{z}/{x}/{y}?access_token=" + AccessToken}
            prefix="" attribution={"routing powered by Madrich"}>
         </TileLayer>

         {/*
             deplist.map((element) =>
               //(element: Depot) => 
               <Marker icon={depotIcon} position={element.coordinates} className="MapDepoMarker">
                  <Popup>
                     <span>{"Склад: " + element.name}</span>
                  </Popup>
               </Marker>
            )
         */}
        
      </MapContainer>
      
    </div>
   )
}

export default MapView