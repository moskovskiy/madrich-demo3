import './dropdown.css';
import React, {useState} from 'react';

function Dropdown(props) {
    const [show, setShow] = useState(false);
    const [title, setTitle] = useState(props.title)

    let toggle = () => setShow(!show)

    return (
        <div className="dropdown" style = {props.style} >
          <div className="button" onClick={() => toggle()}>
            {title}
          </div>

          { show && <>
            <ul>
              {props.list.map( (el, index, _) =>
                <li index={index}><button className="none" onClick={
                ()=>{
                  props.setSelected(index)
                  toggle()
                  setTitle(props.adder + el)
                }}>
                {props.adder + el}
                </button></li>
              )}
            </ul>
          </> }
       </div>
    );
}

export default Dropdown;
