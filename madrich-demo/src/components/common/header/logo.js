import "./logo.css";

function Logo() {
  return(
    <div className="header__logo">
      MADRICH
    </div>
  );
}

export default Logo;
