import React from 'react'
import {useState} from 'react'
import './button.css'

export default
function ButtonIco (props) {
  const [state, setState] = useState({
    buttonState: 0
  });

    return (
        <div
            className="ButtonIco"
            style={props.style}
            onClick={props.onClick}
        >
            <div className="button-title">{props.title}</div>
            {
                props.ico != 0 &&
                  <div className="small-ico">{
                    props.ico[state.buttonState]
                  }</div>
            }
        </div>
    )
}
