import './index.css'
import './fonts/index.css'

import React from 'react'
import ReactDOM from 'react-dom'
import reportWebVitals from './reportWebVitals'

import Header from './components/common/header/header.js'
import Upload from './components/views/upload/upload.js'
import Results from './components/views/results/results.js'
import Demo from './components/views/results/demo'

ReactDOM.render(
  <React.StrictMode>
    {/*
    <Demo />
    */}
    
    <Header />

    <Upload/>
    <Results/>
    
    
  </React.StrictMode>,
  document.getElementById('root')
);

reportWebVitals(console.log);

localStorage.setItem('job_id', null)
